package com.airline.cosmeticsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CosmeticsManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CosmeticsManagerApplication.class, args);
    }

}
