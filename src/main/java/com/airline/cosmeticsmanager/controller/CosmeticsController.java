package com.airline.cosmeticsmanager.controller;

import com.airline.cosmeticsmanager.model.CosmeticsRequest;
import com.airline.cosmeticsmanager.service.CosmeticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cosmetics")
public class CosmeticsController {
    private final CosmeticsService cosmeticsService;

    @PostMapping("/data")
    public  String setCosmetics(@RequestBody CosmeticsRequest request) {
        cosmeticsService.setCosmetics(request.getCosmeticsName(), request.getCosmeticsType(), request.getCosmeticsowner());

        return "ok";
    }
}
