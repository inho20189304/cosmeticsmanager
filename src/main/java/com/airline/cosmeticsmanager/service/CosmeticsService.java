package com.airline.cosmeticsmanager.service;

import com.airline.cosmeticsmanager.entity.Cosmetics;
import com.airline.cosmeticsmanager.repository.CosmeticsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CosmeticsService {
    private final CosmeticsRepository cosmeticsRepository;

    public void setCosmetics(String cosmetics_Name, String cosmetics_Type,String cosmetics_owner) {
        Cosmetics addData = new Cosmetics();
        addData.setCosmeticsName(cosmetics_Name);
        addData.setCosmeticsType(cosmetics_Type);
        addData.setCosmeticsOwner(cosmetics_owner);

        cosmeticsRepository.save(addData);
    }
}
