package com.airline.cosmeticsmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Cosmetics {
    @Id // 명찰
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String cosmeticsName;

    @Column(nullable = false, length = 20)
    private String cosmeticsType;

    @Column(nullable = false, length = 20)
    private String cosmeticsOwner;


}
