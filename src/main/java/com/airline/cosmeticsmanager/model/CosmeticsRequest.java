package com.airline.cosmeticsmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CosmeticsRequest {

    private String cosmeticsName;
    private String cosmeticsType;
    private String cosmeticsowner;
}
