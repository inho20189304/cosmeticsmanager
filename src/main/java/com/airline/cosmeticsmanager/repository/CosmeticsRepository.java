package com.airline.cosmeticsmanager.repository;

import com.airline.cosmeticsmanager.entity.Cosmetics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticsRepository extends JpaRepository<Cosmetics, Long> {
}
